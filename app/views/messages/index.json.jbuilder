json.array!(@messages) do |message|
  json.extract! message, :id, :from_uid, :to_uid, :text, :date, :subject
  json.url message_url(message, format: :json)
end
