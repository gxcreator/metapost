class MpapiController < ApplicationController
  layout  false
  def get

    @trk = {}
    @hash = {}

    @tracks = Track.all

    @tracks.each do |track|
      @markers = Marker.where(tid: track.id)
      @markerData = Gmaps4rails.build_markers(@markers) do |marker, mrk|
        mrk.lat marker.lat
        mrk.lng marker.lng
      end
      @trk = {track.id => {descr: track.about, start:track.start, markers: @markerData}}
      puts @trk.to_json
      @hash.merge!(@trk)
    end

  #  @hash = {}

  end

  def add

    tmptrack = Track.new

    @mrk = JSON.parse(params[:markers])

    tmptrack.uid = current_user.id
    tmptrack.start = params[:start]
    tmptrack.about = params[:descr]

    tmptrack.save

    @mrk.each do |mark|
      tmpMark = Marker.new
      tmpMark.lat = mark["mb"]
      tmpMark.lng = mark["nb"]
      tmpMark.tid = tmptrack.id
      tmpMark.save
    end

  end

end
