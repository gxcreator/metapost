class CreateMessages < ActiveRecord::Migration
  def change
    create_table :messages do |t|
      t.integer :from_uid
      t.integer :to_uid
      t.string :text
      t.datetime :date
      t.string :subject

      t.timestamps
    end
  end
end
