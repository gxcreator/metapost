class CreateMarkers < ActiveRecord::Migration
  def change
    create_table :markers do |t|
      t.integer :tid
      t.float :lat
      t.float :lng
      t.integer :status
      t.float :radius

      t.timestamps
    end
  end
end
