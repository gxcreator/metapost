class CreateTracks < ActiveRecord::Migration
  def change
    create_table :tracks do |t|
      t.integer :uid
      t.text :about
      t.datetime :date
      t.integer :period
      t.datetime :start
      t.float :distance
      t.text :status

      t.timestamps
    end
  end
end
