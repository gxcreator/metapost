class FixColumnName < ActiveRecord::Migration
  def change
    rename_column :messages, :date, :msgdate
  end
end
